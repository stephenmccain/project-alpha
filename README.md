# Project And Task Manger Application
- Week 6 Module 1 project
- Application built with Python and Django
- Creation of projects, tasks and as well as login/logout/sign up features
- Based on HTML templates and template inheritance
- Models include Project and Task
- Includes Create, Detail, Update and List views